import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadPosts = createAsyncThunk(
  ActionType.SET_ALL_POSTS,
  async (filters, { extra: { services } }) => {
    const posts = await services.post.getAllPosts(filters);
    return { posts };
  }
);

const loadMorePosts = createAsyncThunk(
  ActionType.LOAD_MORE_POSTS,
  async (filters, { getState, extra: { services } }) => {
    const {
      posts: { posts }
    } = getState();
    const loadedPosts = await services.post.getAllPosts(filters);
    const filteredPosts = loadedPosts.filter(
      post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );

    return { posts: filteredPosts };
  }
);

const applyPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (postId, { extra: { services } }) => {
    const post = await services.post.getPost(postId);
    return { post };
  }
);

const createPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (post, { extra: { services } }) => {
    const { id } = await services.post.addPost(post);
    const newPost = await services.post.getPost(id);

    return { post: newPost };
  }
);

const loadExpandedPost = createAsyncThunk(
  ActionType.SET_EXPANDED_POST,
  async (postId, { extra: { services } }) => {
    const post = postId ? await services.post.getPost(postId) : undefined;
    return { post };
  }
);

const likePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const checkDislike = async () => {
      const reaction = await services.post.getPostReaction(postId);
      if (reaction?.isLike === false) {
        return -1;
      }
      return 0;
    };
    const removeDislike = await checkDislike();

    const { id } = await services.post.likePost(postId);
    const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
    const mapLikes = post => ({
      ...post,
      likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
      dislikeCount: Number(post.dislikeCount) + removeDislike
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post =>
      post.id !== postId ? post : mapLikes(post)
    );
    const updatedExpandedPost =
      expandedPost?.id === postId ? mapLikes(expandedPost) : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const dislikePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const checkLike = async () => {
      const reaction = await services.post.getPostReaction(postId);
      if (reaction?.isLike === true) {
        return -1;
      }
      return 0;
    };
    const removeLike = await checkLike();

    const { id } = await services.post.dislikePost(postId);
    const diff = id ? 1 : -1; // if ID exists then the post was disliked, otherwise - like was removed

    const mapDislikes = post => ({
      ...post,
      dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
      likeCount: Number(post.likeCount) + removeLike
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post =>
      post.id !== postId ? post : mapDislikes(post)
    );
    const updatedExpandedPost =
      expandedPost?.id === postId ? mapDislikes(expandedPost) : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const deletePost = createAsyncThunk(
  ActionType.DELETE_POST,
  async(postId, {getState, extra: { services } }) => {
    await services.post.deletePost(postId);
    const { 
      posts: {posts}
    } = getState();
    const updated = posts.filter(post => post.id !== postId);
    return { posts: updated};
  }
);

const updatePost = createAsyncThunk(
  ActionType.UPDATE_POST,
  async ({ postId, imageId, body }, { getState, extra: { services } }) => {
    await services.post.updatePost(postId, {
      imageId,
      body
    });

    const {
      posts: { posts}
    } = getState();

    const updatePost = await services.post.getPost(postId);
    const updatedPosts = posts.filter(post => post.id !== postId);

    updatedPosts.unshift(updatePost);

    return {posts: updatedPosts};
  }
); 

const addComment = createAsyncThunk(
  ActionType.COMMENT,
  async (request, { getState, extra: { services } }) => {
    const { id } = await services.comment.addComment(request);
    const comment = await services.comment.getComment(id);

    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) + 1,
      comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

    const updatedExpandedPost = expandedPost?.id === comment.postId
      ? mapComments(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

export {
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  loadExpandedPost,
  likePost,
  dislikePost,
  addComment,
  deletePost,
  updatePost
};
