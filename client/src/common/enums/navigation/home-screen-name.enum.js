const HomeScreenName = {
  THREAD: 'Thread',
  EXPANDED_POST: 'Expanded Post',
  UPDATE_POST: 'Update Post'
};

export { HomeScreenName };
